# -*- coding: utf-8 -*-
import logging
import re
import string
import unicodedata
from lxml import etree

def append_child_element(parent, tag, text = None, tail = "\n"):
	child = etree.Element(tag)
	child.text = text
	child.tail = tail
	parent.append(child)
	return child

def replace_element(old_element, new_element):
	parent = old_element.getparent()
	if old_element.tail:
		if new_element.tail:
			new_element.tail += old_element.tail
		else:
			new_element.tail = old_element.tail
	parent.replace(old_element, new_element)

def strip_all_after(e):
	if e is None:
		return
	parent = e.getparent()
	while e is not None:
		next = e.getnext()
		parent.remove(e)
		e = next

def strip_element(e, leave_text =False):
	if leave_text and e.text:
		insert_text(e, e.text)
	parent = e.getparent()
	tail = e.tail
	if tail and not tail.isspace():
		insert_text(e, tail)
	parent.remove(e)
	return parent

def strip_elements(elements, leave_text =False):
	for e in elements:
		strip_element(e, leave_text)

def strip_element_and_empty_ascendants(e, leave_text =False):
	parent = strip_element(e, leave_text=leave_text)
	while len(parent.getchildren()) == 0 and (not parent.text or parent.text.isspace()):
		parent = strip_element(parent, leave_text=leave_text)

def insert_text(element, text):
	if not text:
		return
	prev = element.getprevious()
	if prev is not None:
		if prev.tail:
			prev.tail += text
		else:
			prev.tail = text
		return
	parent = element.getparent()
	if parent.text is not None:
		parent.text += text
	else:
		parent.text = text

def add_class(element, class_name):
	current = element.attrib.get('class')
	if current:
		class_name = current + ' ' + class_name
	element.attrib['class'] = class_name

def replace_align_to_class(root):
	for center in root.findall('.//center'):
		center.tag = "div"
		add_class(center, "center")
	for e in root.findall('.//*[@align]'):
		align = e.attrib['align']
		if align != 'center' and align != 'right' and align != 'justify':
			continue
		del e.attrib['align']
		add_class(e, align)

def pre_wrap_to_p(element, concat =False):
	lines = text_content(element).strip().split('\n')
	if not lines:
		return
	if concat:
		lines = concat_pre_wrap_lines(lines)
	append_lines_as_p(element, lines)
	element.text = '\n'

def text_content(element):
	text =  ''.join(text_content_list(element, []))
	return text

def text_content_list(element, lines):
	if element.text:
		lines.append(element.text)
	for child in element.getchildren():
		text_content_list(child, lines)
	if element.tail:
		lines.append(element.tail)
	return lines

def concat_pre_wrap_lines(lines):
	continuations = []
	for line in lines:
		if continuations and (not line
			or unicodedata.category(unicode(line[0]))[0] not in 'LN'
			or unicodedata.category(unicode(continuations[-1][-1]))[0] not in 'LN'):
			yield ''.join(continuations)
			continuations = []
		if not line:
			yield line
			continue
		continuations.append(line)
	if continuations:
		yield ''.join(continuations)

def append_lines_as_p(element, lines):
	for line in lines:
		append_child_element(element, "p", line)

def hyphens_to_hr(element):
	for br in element.findall('br'):
		text = br.tail
		if text and re.match(r'^[-=]{5,}$', text):
			br.tag = 'hr'
			insert_text(br, '\n')
			br.tail = '\n'
			if text[0] == '=':
				add_class(br, 'double')
			continue
		prev = br.getprevious()
		if prev is not None and prev.tag == 'hr' and (prev.tail is None or prev.tail.isspace()):
			strip_element(br)

def verticalize_text(element):
	if element.tag == etree.Comment:
		return
	element.text, child_index = _verticalize_text(element, element.text, 0)
	children = element.getchildren()
	if len(children) <= child_index:
		return
	child = children[child_index]
	while child is not None:
		verticalize_text(child)
		next = child.getnext()
		child_index += 1
		if child.tail:
			child.tail, child_index = _verticalize_text(element, child.tail, child_index)
		child = next

def _verticalize_text(element, text, child_index):
	if not text:
		return text, child_index
	text = text.replace("!?", u"⁉︎")
	ranges = list(_verticalize_text_ranges(text))
	count = len(ranges)
	if count == 0:
		return text, child_index
	# logging.info("tcy <%s>%s => %s", element.tag, text, ranges)
	for i, r in enumerate(ranges):
		span = etree.Element('span')
		span.attrib['class'] = r[2]
		span.text = r[3]
		span.tail = text[r[1]:(ranges[i+1][0] if i < count - 1 else len(text))]
		element.insert(child_index, span)
		child_index += 1
	return text[0:ranges[0][0]], child_index

def _verticalize_text_ranges(text):
	i = 0
	while i < len(text):
		c = text[i]
		if c >= '0' and c <= '9':
			end = _find_not_in_min_max(text, i + 1, '0', '9')
			if end - i <= 4:
				yield i, end, "tcy", text[i:end]
			i = end
			continue
		if c >= u'\uFF10' and c <= u'\uFF19':
			end = _find_not_in_min_max(text, i + 1, u'\uFF10', u'\uFF19')
			if end - i == 2:
				yield (i, end, "tcy",
					''.join(map(lambda c: chr(ord(c) - (0xFF10 - 0x30)) if c >= u'\uFF10' else c,
					text[i:end])))
			i = end
			continue
		if c in string.ascii_letters:
			end = _find_not_in(text, i + 1, lambda c: c in string.ascii_letters)
			if end - i == 1:
				yield i, end, "tcy", text[i]
			i = end
			continue
		if c in u"\u201C\u201D":
			yield i, i + 1, "u", text[i]
		i += 1

def _find_not_in_min_max(str, i, min, max):
	return _find_not_in(str, i, lambda c: c >= min and c <= max)

def _find_not_in(str, i, f):
	while i < len(str):
		if not f(str[i]):
			return i
		i += 1
	return i

def full_width_digits_to_half_width(text):
	return ''.join(map(lambda c: chr(ord(c) - (0xFF10 - 0x30)) if c >= u'\uFF10' else c, text))
