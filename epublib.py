import codecs
import datetime
import logging
import mimetypes
import os
import re
import StringIO
import sys
import uuid
import urlparse
import zipfile
from lxml import etree

logger = logging.getLogger('epublib')

class EpubItem(object):
	def __init__(self, path):
		self.id = None
		self.path = path
		self.mimetype = None
		self.manifest_properties = []
		self._is_spine = False
		self.spineProperties = []

	def mimetype_or_guess(self):
		mimetype = self.mimetype
		if mimetype:
			return mimetype
		path = self.path

		if path.endswith('.jpg'): return 'image/jpeg' # mimetypes.guess_type returns 'image/pjpeg'
		if path.endswith('.png'): return 'image/png' # mimetypes.guess_type returns 'image/x-png'
		if path.endswith('.svg'): return 'image/svg+xml'
		if path.endswith('.xhtml'): return 'application/xhtml+xml'
		path = self.local_path
		if path:
			return mimetypes.guess_type(path)[0]
		assert False
		return None

	@property
	def is_spine(self):
		return self._is_spine

	@is_spine.setter
	def is_spine(self, value):
		self._is_spine = value

	def set_is_spine(self):
		self.is_spine = True
		return self

	def isImage(self):
		mimetype = self.mimetype_or_guess()
		if mimetype.startswith('image/'): return True
		return False

	@property
	def is_navigation(self):
		return 'nav' in self.manifest_properties

	@is_navigation.setter
	def is_navigation(self, value):
		if not value:
			self.manifest_properties.remove('nav')
		elif not 'nav' in self.manifest_properties:
			self.manifest_properties.append('nav')

	def add_manifest_property(self, property):
		self.manifest_properties.append(property)
		return self

	def addSpineProperty(self, property):
		self.spineProperties.append(property)
		return self

	def setCoverImage(self):
		assert self.isImage()
		return self.add_manifest_property('cover-image')

	def setPageSpread(self, spread):
		if spread == 'left' or spread == 'right':
			self.addSpineProperty('page-spread-' + spread)
		elif spread == 'center':
			self.addSpineProperty('rendition:page-spread-' + spread)
		else:
			raise ValueError, 'Value "%s" not supported' % spread

	def wrapWithSvg(self, width, height):
		assert self.isImage()
		svg = os.path.splitext(self.path)[0] + '.svg'
		item = EpubStringItem(svg,
			'<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"'
			' viewBox="0 0 %d %d"'
			' version="1.1" width="100%%" height="100%%"'
			' preserveAspectRatio="xMidYMid meet"'
			'>\n'
			'<image width="%d" height="%d" xlink:href="%s" />'
			'</svg>\n' % (width, height, width, height, os.path.basename(self.path)))
		return item

	def wrapWithXhtml(self, width = 0, height = 0):
		assert self.isImage()
		xhtml = os.path.splitext(self.path)[0] + '.xhtml'
		item = EpubWriteableItem(xhtml)
		item.writer.write(
			'<?xml version="1.0" encoding="UTF-8"?>\n'
			'<!DOCTYPE html>\n'
			'<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" xml:lang="ja">\n'
			'<head>\n'
			'<meta charset="UTF-8" />\n'
			'<title></title>\n'
			'<style type="text/css">\n'
			'img.fit { display:block; margin:auto; height:auto; width:auto; max-width:100%; max-height:100%; }\n'
			'body { text-align:center; background-color: #eeeeee; margin: 0px; }\n'
			'</style>\n')
		if width > 0 and height > 0:
			item.writer.write('<meta name="viewport" content="width=%d, height=%d" />\n' % (width, height))
		item.writer.write(
			'</head>\n'
			'<body>\n'
			'<img src="%s" class="fit" alt="" />\n'
			'</body>\n'
			'</html>\n' % (os.path.basename(self.path)))
		return item

	def wrapWithXhtmlSvg(self, width, height):
		assert self.isImage()
		xhtml = os.path.splitext(self.path)[0] + '.xhtml'
		item = EpubWriteableItem(xhtml)
		item.writer.write(
			'<?xml version="1.0" encoding="UTF-8"?>\n'
			'<!DOCTYPE html>\n'
			'<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" xml:lang="ja">\n'
			'<head>\n'
			'<meta charset="UTF-8" />\n'
			'<title></title>\n'
			'<style type="text/css">\n'
			'html,body { margin:0; padding:0; font-size:0; }\n'
			'svg { margin:0; padding:0; }\n'
			'</style>\n'
			'<meta name="viewport" content="width=%d, height=%d" />\n'
			'</head>\n'
			'<body>\n'
			'<div>\n'
			'<svg xmlns="http://www.w3.org/2000/svg" version="1.1"\n'
			' xmlns:xlink="http://www.w3.org/1999/xlink"\n'
			' width="100%%" height="100%%" viewBox="0 0 %d %d">\n'
			'<image width="%d" height="%d" xlink:href="%s"/>\n'
			'</svg>\n'
			'</div>\n'
			'</body>\n'
			'</html>\n' % (width, height, width, height, width, height, os.path.basename(self.path)))
		item.add_manifest_property('svg')
		return item

	def writeOpfItemTo(self, file):
		path = self.path
		if path.startswith('EPUB/'):
			path = path[5:]
		else:
			path = '../' + path
		file.write('<item id="%s" href="%s" media-type="%s"' % (self.id, path, self.mimetype_or_guess()))
		props = self.manifest_properties
		if props:
			file.write(' properties="%s"' % ' '.join(props))
		file.write('/>\n')

	def writeOpfSpineTo(self, file):
		if not hasattr(self, 'is_spine') or not self.is_spine:
			return
		file.write('<itemref idref="%s"' % (self.id))
		props = self.spineProperties
		if props:
			file.write(' properties="%s"' % ' '.join(props))
		file.write('/>\n')

class EpubFileItem(EpubItem):

	def __init__(self, path, local_path, mimetype = None):
		EpubItem.__init__(self, path)
		self.local_path = local_path
		if mimetype:
			self.mimetype = mimetype

	def writeToArchive(self, archive):
		assert isinstance(archive, zipfile.ZipFile)
		archive.write(self.local_path, self.path, compress_type = zipfile.ZIP_DEFLATED)

	def get_root_element(self):
		return etree.parse(self.local_path).getroot()

class EpubStringItem(EpubItem):

	def __init__(self, path, str):
		EpubItem.__init__(self, path)
		self._string = str

	def writeToArchive(self, archive, compress_type = zipfile.ZIP_DEFLATED):
		assert isinstance(archive, zipfile.ZipFile)
		archive.writestr(self.path, self._string, compress_type)

class EpubWriteableItem(EpubItem):

	def __init__(self, path):
		EpubItem.__init__(self, path)
		self._file = StringIO.StringIO()
		self.writer = codecs.getwriter('utf8')(self._file)

	def close(self):
		if not self._file:
			return
		self._string = self._file.getvalue()
		self._file.close()
		self._file = None
		self.writer = None

	def writeToArchive(self, archive, compress_type = zipfile.ZIP_DEFLATED):
		self.close()
		assert isinstance(archive, zipfile.ZipFile)
		archive.writestr(self.path, self._string, compress_type)

class EpubFile(object):
	def __init__(self, path):
		self.uid = None
		self.title = None
		self.items = []
		self._items_by_path = None
		self.prefixes = {}
		self.properties = {}
		self._page_progression_direction = None
		self.__idCount = 0
		self.path = path

	def setPrefix(self, name, value):
		self.prefixes[name] = value
		return self

	def setRenditionPrefix(self):
		return self.setPrefix('rendition', 'http://www.idpf.org/vocab/rendition/#')

	def __checkRenditionPrefix(self):
		if 'rendition' in self.prefixes:
			return
		for prop in self.properties.iterkeys():
			if prop.startswith('rendition:'):
				self.setRenditionPrefix()
				return
		for item in self.items:
			for prop in item.spineProperties:
				if prop.startswith('rendition:'):
					self.setRenditionPrefix()
					return

	def setProperty(self, name, value):
		self.properties[name] = value
		return self

	@property
	def page_progression_direction(self):
		return self._page_progression_direction

	@page_progression_direction.setter
	def page_progression_direction(self, value):
		self._page_progression_direction = value

	def setFixedLayout(self):
		self.setProperty('rendition:layout', 'pre-paginated')
		return self

	def add_item(self, item):
		if isinstance(item, basestring):
			item = EpubFileItem('EPUB/' + os.path.basename(item), item)
		if not item.id:
			self.__idCount += 1
			item.id = "i" + str(self.__idCount)
		self.items.append(item)
		if self._items_by_path:
			assert not item.path in self._items_by_path
			self._items_by_path[item.path] = item
		return item

	def addSpine(self, item):
		item = self.add_item(item)
		return item.setIsSpine()

	def addJpgSpine(self, item):
		item = self.addSpine(item)
		#item.mimetype = 'image/jpeg'
		return item

	def item_from_path(self, path):
		if not self._items_by_path:
			self._items_by_path = {}
			for i in self.items:
				self._items_by_path[i.path] = i
		return self._items_by_path.get(path, None)

	def save(self):
		archive = zipfile.ZipFile(self.path, 'w')

		item = EpubStringItem('mimetype', 'application/epub+zip')
		item.writeToArchive(archive, compress_type = zipfile.ZIP_STORED)

		item = EpubWriteableItem('META-INF/container.xml')
		self.writeContainerTo(item.writer)
		item.writeToArchive(archive)
		item.close()

		self._check_navigation_or_add_default()

		item = EpubWriteableItem('EPUB/package.opf')
		self.writeOpfTo(item.writer)
		#self.writeOpfTo(sys.stdout)
		item.writeToArchive(archive)
		item.close()

		for item in self.items:
			item.writeToArchive(archive)

		archive.close()

	def writeContainerTo(self, file):
		file.write('<?xml version="1.0" encoding="UTF-8"?>\n')
		file.write('<container xmlns="urn:oasis:names:tc:opendocument:xmlns:container" version="1.0">\n')
		file.write('<rootfiles>\n')
		file.write('<rootfile full-path="EPUB/package.opf" media-type="application/oebps-package+xml"/>\n')
		file.write('</rootfiles>\n')
		file.write('</container>\n')

	def writeOpfTo(self, file):
		file.write('<?xml version="1.0" encoding="UTF-8"?>\n')
		file.write('<package xmlns="http://www.idpf.org/2007/opf" version="3.0" unique-identifier="uid"')
		self.__checkRenditionPrefix()
		prefixes = self.prefixes
		if prefixes:
			file.write(' prefix="%s"' % ' '.join(['%s: %s' % kv for kv in prefixes.iteritems()]))
		file.write('>\n');

		file.write('<metadata xmlns:dc="http://purl.org/dc/elements/1.1/">\n')
		if not self.uid: self.uid = uuid.uuid1().urn
		file.write('<dc:identifier id="uid">%s</dc:identifier>\n' % self.uid)
		file.write('<dc:title id="title">%s</dc:title>\n' % self.title)
		file.write('<dc:language>ja</dc:language>\n')
		file.write('<meta property="dcterms:modified">%sZ</meta>\n' % datetime.datetime.utcnow().strftime('%Y-%m-%dT%H:%M:%S'))
		for kv in self.properties.iteritems():
			file.write('<meta property="%s">%s</meta>\n' % kv)
		file.write('</metadata>\n')

		file.write('<manifest>\n')
		for item in self.items:
			item.writeOpfItemTo(file)
		file.write('</manifest>\n')

		file.write('<spine')
		page_progression_direction = self.page_progression_direction
		if page_progression_direction:
			file.write(' page-progression-direction="%s"' % page_progression_direction)
		file.write('>\n')
		for item in self.items:
			item.writeOpfSpineTo(file)
		file.write('</spine>\n')

		file.write('</package>\n')

	@property
	def navigation_item(self):
		for item in self.items:
			if item.is_navigation:
				return item
		return None

	def add_navigation_item(self, item):
		item = self.add_item(item)
		item.is_navigation = True
		return item

	def _check_navigation_or_add_default(self):
		if self.navigation_item:
			return
		item = EpubWriteableItem('EPUB/nav.xhtml')
		self.writeNavTo(item.writer)
		item.close()
		item.add_manifest_property('nav')
		self.add_item(item)

	def writeNavTo(self, file):
		file.write('<?xml version="1.0" encoding="utf-8"?>\n')
		file.write('<!DOCTYPE html>\n')
		file.write('<html xmlns="http://www.w3.org/1999/xhtml" xmlns:epub="http://www.idpf.org/2007/ops" lang="ja-JP" xml:lang="ja-JP">\n')
		file.write('<head>\n')
		file.write('<title>%s</title>\n' % self.title)
		file.write('</head>\n')
		file.write('<body>\n')
		file.write('<nav epub:type="toc" id="toc">\n')
		file.write('<h1>%s</h1>\n' % self.title)
		file.write('<ol>\n')
		#file.write('<li><a href="0001.xhtml"></a></li>\n')
		file.write('</ol>\n')
		file.write('</nav>\n')
		file.write('</body>\n')
		file.write('</html>\n')

	def add_linked_items(self, item):
		try:
			root = item.get_root_element()
		except:
			logging.error('Failed to load XHTML, ignored')
			return
		for link in root.findall('.//{http://www.w3.org/1999/xhtml}link[@rel="stylesheet"]'):
			self._add_href(item, link.attrib['href'])
		for a in root.findall('.//{http://www.w3.org/1999/xhtml}a[@href]'):
			added = self._add_href(item, a.attrib['href'])
			if not added:
				continue
			if not('target' in a.attrib and a.attrib['target'] == '_blank'):
				added.is_spine = True
			self.add_linked_items(added)
		for img in root.findall('.//{http://www.w3.org/1999/xhtml}img'):
			self._add_href(item, img.attrib['src'])

	def _add_href(self, item, href):
		components = urlparse.urlparse(href)
		if components.scheme or components.path.startswith('/'):
			logging.warning('add_linked_items: Absolute URL ignored: %s', href)
			return
		path = urlparse.urljoin(item.path, href)
		if self.item_from_path(path):
			return None
		local_path = urlparse.urljoin(item.local_path, href)
		logger.debug("add_linked_items %s %s", path, local_path)
		item = self.add_item(EpubFileItem(path, local_path))
		return item
